import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { Blockchain } from '../shared/class/blockchain';
import { UserService } from '../shared/services/user.service';
import { log } from 'util';
import { element } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public hashId = new FormControl('', [Validators.required]);
  public password = new FormControl('', [Validators.required]);
  constructor(
    public router: Router,
    public blockchain: Blockchain,
    public userService: UserService
  ) { }
public test;
  ngOnInit() {
    if(this.userService.isLoggedIn()) {
      this.router.navigateByUrl('/dashboard')
    }
    // this.userService.create({name: 'duoc', age: 12}, 'users');
    // this.userService.update({name: 'Tuong Vi Nguyen'}, 'users', '-LQT4j-oRtE1WdYO0Bwz');
    this.userService.getList().snapshotChanges().subscribe(item => {
      this.test = [];
      item.forEach(element => {
        let y = element.payload.toJSON();
        y["key"] = element.key;
        this.test.push(y);
      })
      // console.log(item)
      console.log(this.test);
    })
  }


  getErrorMessage() {
    return this.hashId.hasError('required') ? 'You must enter a value' :
        this.password.hasError('required') ? 'You must enter a value' :
          '';
  }

  login() {
    // 0xCdE8cA8EEd88501B55f4Cae6D8e534b02A1Dd5B9
    // 0x9ecff6d02d0da2f6ba6a75a9dd642a6e32942b16
  
    if (this.getErrorMessage() === '') {
      let check = this.userService.logIn(this.hashId.value, this.password.value);
      if(check) {
        this.userService.getList().snapshotChanges().subscribe(
          item => {
            let length = item.length;
            for (let i = 0; i < length; i++) {
              if (item[i].payload.toJSON()['hash'] === localStorage.getItem('hashId') ) {
                if( item[i].payload.toJSON()['role'] == 1) {
                  this.router.navigateByUrl('admin/dashboard');
                } else {
                  this.router.navigateByUrl('dashboard');
                }
                break;
              }
            }
          })
        // this.router.navigateByUrl('/dashboard');
      } else {
        alert("Đăng nhập thất bại")
      }
    }
  }
}
