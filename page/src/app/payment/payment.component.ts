import { Component, OnInit } from '@angular/core';
import { Menu } from './../shared/class/menu';
import { UserService } from '../shared/services/user.service';
import * as _ from 'lodash'; 
import { Router } from '@angular/router';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  public step: number = 1;
  public user;
  public listPayment = [];
  constructor(
    public menu: Menu,
    public userService: UserService,
    public router: Router
  ) { }

  ngOnInit() {
    this.menu.currentTitle = 'Thanh toán';
    this.getItem();
  }
  getItem() {
    this.userService.getList().snapshotChanges().subscribe(
      item => {
        let length = item.length;
        for (let i = 0; i < length; i++) {
          if (item[i].payload.toJSON()['hash'] === localStorage.getItem('hashId')) {
            this.user = item[i].payload.toJSON();
            this.user.id = item[i]['key'];
            _.forEach(this.user.payment, (value, key) => {
              let temp = value;
              temp['id'] = key;
              this.listPayment.push(temp);
            });
            break;
          }
        }
      })
      // this.listPayment = this.user.payment;
  }


  pay(payment) {
    if(payment.status == 0) {
      this.router.navigateByUrl(`/payment/${this.user.id}/${payment.id}`);
    } else {
      alert('Bạn đã thanh toán rồi!');
    }
  }



}
