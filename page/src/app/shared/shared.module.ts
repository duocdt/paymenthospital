import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './services/user.service';
import { Blockchain } from './class/blockchain';
import { Menu } from './class/menu';
import { User } from './class/user';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    Menu,
    Blockchain,
    UserService,
    User,
    AuthGuard
  ]
})
export class SharedModule { }
