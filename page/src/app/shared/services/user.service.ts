import { Injectable } from '@angular/core';
import { Blockchain } from '../class/blockchain';
import { User } from '../class/user';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { log } from 'util';
import { element } from 'protractor';
// import { Headers, Http, Response, RequestOptions } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private field = 'users';
  constructor(
    public blockchain: Blockchain,
    public user: User,
    public router: Router,
    // protected http: Http,
    protected db?: AngularFireDatabase
  ) { }

  logIn(hashId, password) {
    try {
      let check = this.blockchain.web3.personal.unlockAccount(hashId, password);
      if (check) {
        let code = hashId + ':' + password;
        // Mã hóa base64 2 lần
        let token = window.btoa(window.btoa(code));
        localStorage.setItem('token', token);
        localStorage.setItem('hashId', hashId);
        this.getList().snapshotChanges().subscribe(
          item => {
            let user;
            let length = item.length;
            for (let i = 0; i < length; i++) {
              if (item[i].payload.toJSON()['hash'] === hashId) {
                user = item[i].payload.toJSON();
                user['key'] = item[i].key;
                break;
              }
            }
            if (!user) {
              this.create('users',{
                name: '',
                username: '',
                age: '',
                job: '',
                birthday: '',
                phone: '',
                address: '',
                hash: hashId
              })
            }
          }
        )

        return true;
      }
    } catch (error) {
      return false;
    }
  }

  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('hashId');
    this.user.isLoggedIn = false;
    this.router.navigateByUrl('/login');

  }

  isLoggedIn() {
    let token = localStorage.getItem('token');
    let hashIdFromLocal = localStorage.getItem('hashId');
    if (token && hashIdFromLocal) {
      // console.log(token);
      // console.log(hashIdFromLocal)
      //  Giải mã base64
      let code = window.atob(window.atob(token));
      let hashId = code.split(":")[0];
      let password = code.split(":")[1];
      try {
        let check = this.blockchain.web3.personal.unlockAccount(hashId, password);
        if (check) {
          return true;
        }
      } catch (error) {
        return false;
      }
    }
    return false;
  }

  // firebase
  getList() {
    return this.db.list(this.field);
  }

  getItem(link) {
    return this.db.object(link);
  }


  create(link, data) {
    return this.db.list(link).push(data);
  }



  update(link,data, key) {
    this.db.list(link).update(key, data);
  }

}
