import Web3 from 'web3';
export class Blockchain {
    public web3;
    public balance = 0;
    public hashIdHospital = '0x8ea9f88ce85647b55c0fad5bf0a3323383d8f969';
    public ABIcontract = [ { "constant": true, "inputs": [], "name": "name", "outputs": [ { "name": "", "type": "string", "value": "Duoc" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_spender", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "approve", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [ { "name": "", "type": "uint256", "value": "1e+22" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "transferFrom", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [ { "name": "", "type": "uint8", "value": "18" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_value", "type": "uint256" } ], "name": "burn", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" } ], "name": "balanceOf", "outputs": [ { "name": "", "type": "uint256", "value": "0" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_from", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "burnFrom", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [ { "name": "", "type": "string", "value": "DTK" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "transfer", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_spender", "type": "address" }, { "name": "_value", "type": "uint256" }, { "name": "_extraData", "type": "bytes" } ], "name": "approveAndCall", "outputs": [ { "name": "success", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" }, { "name": "", "type": "address" } ], "name": "allowance", "outputs": [ { "name": "", "type": "uint256", "value": "0" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [ { "name": "initialSupply", "type": "uint256", "index": 0, "typeShort": "uint", "bits": "256", "displayName": "initial Supply", "template": "elements_input_uint", "value": "10000" }, { "name": "tokenName", "type": "string", "index": 1, "typeShort": "string", "bits": "", "displayName": "token Name", "template": "elements_input_string", "value": "Duoc" }, { "name": "tokenSymbol", "type": "string", "index": 2, "typeShort": "string", "bits": "", "displayName": "token Symbol", "template": "elements_input_string", "value": "DTK" } ], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "from", "type": "address" }, { "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" } ], "name": "Transfer", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_owner", "type": "address" }, { "indexed": true, "name": "_spender", "type": "address" }, { "indexed": false, "name": "_value", "type": "uint256" } ], "name": "Approval", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "from", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" } ], "name": "Burn", "type": "event" } ];
    public addContract = '0x8a1cdB08704f461319C2c830D0C66Eb51ef2b79c';
    public contract;
    public events;
    constructor() {

    }
    connect() {
        this.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8080"));
        this.contract = this.web3.eth.contract(this.ABIcontract).at(this.addContract);
        this.events = this.contract.allEvents();
    }

    getBalance() {
        this.balance = this.web3.fromWei(this.web3.eth.getBalance(localStorage.getItem('hashId')));
    }

    sendTransaction(toHashId, value) {
        let fromHashId = localStorage.getItem('hashId');
        let token = localStorage.getItem('token');
        let code = window.atob(window.atob(token));
        let password = code.split(":")[1];
        if (value <= 0 || value > this.balance) {
            alert("Lượng tiền phải lớn hơn 0 và bé hơn số dư cho phép")
        } else {
            this.web3.personal.unlockAccount(fromHashId, password);
            this.web3.eth.sendTransaction(
                {
                    from: fromHashId,
                    to: toHashId,
                    value: value * Math.pow(10, 18)
                }, function (error, result) {
                    if (!error) {
                        alert(`Bạn đã chuyển ${value} ETH từ ${fromHashId} đến ${toHashId}`);
                    }
                    else {
                        alert("Đã xảy ra lỗi, giao dịch không được thực hiện");
                    }
                }
            )
        }

    }

    // trả về số block hiện tại
    getBlockNumber() {
        return this.web3.eth.blockNumber;
    }
    // trả về số transaction nhận
    getBlockTransactionCount() {
        return this.web3.eth.getBlockTransactionCount(localStorage.getItem('hashId'));
    }

    // trả về số transaction gửi
    getTransactionCount() {
        return this.web3.eth.getTransactionCount(localStorage.getItem('hashId'))
    }
    
    // trả về thông tin transaction theo transaction hash
    getTransaction(transactionHash) {
        return this.web3.eth.getTransaction(transactionHash);
    }

    // trả về thông tin của block
    getBlock(number) {
        return this.web3.eth.getBlock(number);
    }

    // Lịch sử account
    getTransactionsByAccount() {
        let currentBlock = this.getBlockNumber();
        let hashId = localStorage.getItem('hashId').toLocaleLowerCase();
        let transactions = [];
        for(let i = 0; i < currentBlock; i++) {
            if(this.getBlock(i).transactions.length > 0) {
                let transaction = this.getTransaction(this.getBlock(i).transactions[0]);
                if(transaction.from == hashId || transaction.to == hashId) 
                    transactions.push(transaction) 
            }
        }
        return transactions;
    }

    isConnected() {
        return this.web3.isConnected()
    }
    
}