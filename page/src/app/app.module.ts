import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SidenavComponent } from './layouts/sidenav/sidenav.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './layouts/header/header.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { PaymentComponent } from './payment/payment.component';
import { HistoryComponent } from './history/history.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WalletComponent } from './wallet/wallet.component';
import { RecordComponent } from './record/record.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SharedModule } from './shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AngularFireModule} from 'angularfire2';
import { AngularFireDatabaseModule} from 'angularfire2/database'
import {environment} from '../environments/environment';
import { RecordEditComponent } from './record-edit/record-edit.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { PaymentCreateComponent } from './admin/payment-create/payment-create.component';
import { PaymentDetailComponent } from './payment-detail/payment-detail.component';
import { PaymentCreateDetailComponent } from './admin/payment-create-detail/payment-create-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidenavComponent,
    HeaderComponent,
    MainLayoutComponent,
    PaymentComponent,
    HistoryComponent,
    DashboardComponent,
    WalletComponent,
    RecordComponent,
    PageNotFoundComponent,
    RecordEditComponent,
    AdminLayoutComponent,
    PaymentCreateComponent,
    PaymentDetailComponent,
    PaymentCreateDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
