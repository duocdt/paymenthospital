import { Component, OnInit } from '@angular/core';
import {Menu} from './../shared/class/menu';
import { Blockchain } from '../shared/class/blockchain';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  public myHashId = localStorage.getItem('hashId');
  public myHashIdLowerCase = localStorage.getItem('hashId').toLocaleLowerCase();
  public transactionsHistory = [];

  constructor(
    public menu: Menu,
    public blockchain: Blockchain,
  ) { }

  ngOnInit() {
    this.menu.currentTitle = 'Lịch sử';
    this.getTransactionsHistory();
  }

  getTransactionsHistory() {
    this.transactionsHistory = this.blockchain.getTransactionsByAccount();
    return this.transactionsHistory;
  }

}
