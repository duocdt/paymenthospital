import { Component, OnInit } from '@angular/core';
import { Blockchain } from './shared/class/blockchain';
import { UserService } from './shared/services/user.service';
import { Router } from '@angular/router';
import { User } from './shared/class/user';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Payment Hospital';
  constructor(
    public blockchain: Blockchain,
    public userService: UserService,
    public router: Router,
    public user: User
  ) {

  }
  ngOnInit() {
    this.blockchain.connect();
  }
  ngDoCheck(): void {

    // 
    // let token = localStorage.getItem('token');
    // let hashIdFromLocal = localStorage.getItem('hashId');
    // console.log(this.user.isLoggedIn);

    // if(this.user.isLoggedIn) {
    //   console.log(0);
    //   if(!token || !hashIdFromLocal || !this.userService.isLoggedIn()) {
    //     console.log(1);
    //     this.userService.logOut();
    //   }
    // }
    
  }
}
