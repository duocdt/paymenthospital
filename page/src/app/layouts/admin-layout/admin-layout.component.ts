import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {

  public listMenu = [
    {
      name: 'Dashboard',
      link: '/admin/dashboard',
      classIcon: 'mdi-home' 
    },
    {
        name: 'Tạo thanh toán',
        link: '/admin/payment-create',
        classIcon: 'mdi-money-box' 
    },
    {
      name: 'Lịch sử',
      link: '/admin/history',
      classIcon: 'mdi-format-list-bulleted' 
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
