import { Component, OnInit, Input } from '@angular/core';
import {Menu} from './../../shared/class/menu';
import { Blockchain } from 'src/app/shared/class/blockchain';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public title: string;
  constructor(
    public menu: Menu,
    public blockchain: Blockchain,
    public userService: UserService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  ngDoCheck(): void {
    this.blockchain.getBalance();
  }

  logOut() {
    this.userService.logOut();
    this.router.navigateByUrl('/login');
  }

}
