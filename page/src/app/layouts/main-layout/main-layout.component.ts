import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  public listMenu = [
    {
      name: 'Dashboard',
      link: '/dashboard',
      classIcon: 'mdi-home' 
    },
    {
      name: 'Thanh toán',
      link: '/payment',
      classIcon: 'mdi-money-box' 
    },
    {
      name: 'Ví',
      link: '/wallet',
      classIcon: 'mdi-balance-wallet' 
    },
    {
      name: 'Lịch sử',
      link: '/history',
      classIcon: 'mdi-format-list-bulleted' 
    },
    {
      name: 'Hồ sơ',
      link: '/record',
      classIcon: 'mdi-assignment-account' 
    },
  ]
  constructor() { }

  ngOnInit() {
  }

}
