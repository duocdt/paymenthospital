
import { Routes, RouterModule } from '@angular/router';
import {NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { PaymentComponent } from './payment/payment.component';
import { HistoryComponent } from './history/history.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WalletComponent } from './wallet/wallet.component';
import { RecordComponent } from './record/record.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { RecordEditComponent } from './record-edit/record-edit.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { PaymentCreateComponent } from './admin/payment-create/payment-create.component';
import { PaymentDetailComponent } from './payment-detail/payment-detail.component';
import { PaymentCreateDetailComponent } from './admin/payment-create-detail/payment-create-detail.component';
export const APP_ROUTES: Routes = [
    {
        path: '',
        component: MainLayoutComponent,
        children: [
            {
                path: 'payment',
                component: PaymentComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'payment/:idUser/:idPayment',
                component: PaymentDetailComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'wallet',
                component: WalletComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'record',
                component: RecordComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'record/edit',
                component: RecordEditComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'history',
                component: HistoryComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'dashboard',
                component: DashboardComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            }
        ]
    },
    {
        path: 'admin',
        component: AdminLayoutComponent,
        children: [
            {
                path: 'history',
                component: HistoryComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'dashboard',
                component: DashboardComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'payment-create',
                component: PaymentCreateComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            },
            {
                path: 'payment-create/:idUser',
                component: PaymentCreateDetailComponent,
                pathMatch: 'full',
                canActivate: [AuthGuard]
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent,
    }
]

@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule{}
