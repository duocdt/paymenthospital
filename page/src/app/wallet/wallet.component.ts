import { Component, OnInit } from '@angular/core';
import {Menu} from './../shared/class/menu';
import { Blockchain } from '../shared/class/blockchain';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {
  public numberTransactionsSent;
  public numberTransactionsReceived;
  public hashId;
  constructor(
    public menu: Menu,
    public blockchain: Blockchain,
  ) { }

  ngOnInit() {
    this.menu.currentTitle = 'Ví';
    this.numberTransactionsSent = this.blockchain.getTransactionCount();
    // this.numberTransactionsReceived = this.blockchain.getBlockTransactionCount();
    this.hashId = localStorage.getItem('hashId');
  }

}
