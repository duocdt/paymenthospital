import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-record-edit',
  templateUrl: './record-edit.component.html',
  styleUrls: ['./record-edit.component.scss']
})
export class RecordEditComponent implements OnInit {
  public user = {
    name: new FormControl(''),
    username: new FormControl(''),
    job: new FormControl(''),
    birthday: new FormControl(''),
    phone: new FormControl(''),
    address: new FormControl(''),
    key: ''
  };
  constructor(
    public userService: UserService,
    public router: Router
  ) { }

  ngOnInit() {
    this.getItem();
  }

  getItem() {
    this.userService.getList().snapshotChanges().subscribe(
      item => {
        let length = item.length;
        for (let i = 0; i < length; i++) {
          if (item[i].payload.toJSON()['hash'] === localStorage.getItem('hashId')) {
            let temp = item[i].payload.toJSON();
            this.user.name.setValue(temp['name']);
            this.user.username.setValue(temp['username']);
            this.user.job.setValue(temp['job']);
            this.user.birthday.setValue(temp['birthday']);
            this.user.phone.setValue(temp['phone']);
            this.user.address.setValue(temp['address']);
            this.user.key = item[i]['key'];
            break;
          }
        }
      })
  }

  save() {
    let body = {
      name: this.user.name.value,
      username: this.user.username.value,
      job: this.user.job.value,
      birthday: this.user.birthday.value,
      phone: this.user.phone.value,
      address: this.user.address.value,
    }
    this.userService.update('users',body,this.user.key);
    this.router.navigateByUrl('/record');
  }

}
