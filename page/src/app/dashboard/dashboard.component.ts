import { Component, OnInit, } from '@angular/core';
import {Menu} from './../shared/class/menu';
import { Blockchain } from '../shared/class/blockchain';
import { FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public hashId = new FormControl('', [Validators.required]);
  public number = new FormControl('', [Validators.required]);
  public myHashId = localStorage.getItem('hashId');
  public myHashIdLowerCase = localStorage.getItem('hashId').toLocaleLowerCase();
  public recentTransactions = [];
  constructor(
    public menu: Menu,
    public blockchain: Blockchain,
    
  ) { }

  ngOnInit() {
    this.menu.currentTitle = 'Dashboard';
    this.getRecentTransactions();
  }

  getErrorMessage() {
    return this.hashId.hasError('required') ? 'You must enter a value' :
        this.number.hasError('required') ? 'You must enter a value' :
          '';
  }

  send() {
    if (this.getErrorMessage() === '') {
      this.blockchain.sendTransaction(this.hashId.value, this.number.value);
      this.reset();
    }
  }

  reset() {
    this.hashId.setValue('');
    this.number.setValue('');
  }

  getRecentTransactions() {
    this.recentTransactions = this.blockchain.getTransactionsByAccount();
    if(this.recentTransactions.length > 5) {
      this.recentTransactions.slice(0, 5);
    }
    return this.recentTransactions;
  }

}
