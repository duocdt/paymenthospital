import { Component, OnInit } from '@angular/core';
import { Menu } from './../shared/class/menu';
import { UserService } from '../shared/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Blockchain } from '../shared/class/blockchain';
@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.scss']
})
export class PaymentDetailComponent implements OnInit {
  public step: number = 1;
  public idUser;
  public idPayment;
  public payment = {};
  public myHashId = localStorage.getItem('hashId');
  constructor(
    public menu: Menu,
    private route: ActivatedRoute,
    public userService: UserService,
    public router: Router,
    public blockchain: Blockchain,
  ) { }

  ngOnInit() {
    this.menu.currentTitle = 'Thanh toán';
    this.route.params.subscribe(params => {
      this.idUser = params['idUser'];
      this.idPayment = params['idPayment'];
    });
    this.getItem();
  }
  getItem() {
    this.userService.getItem(`users/${this.idUser}/payment/${this.idPayment}`).snapshotChanges().subscribe(
      item => {
        this.payment = item.payload.toJSON();
        console.log(this.payment);

      })
  }
  next() {
    if (this.step === 2) {
      this.send();
    } else this.step++;
  }

  send() {
    let self = this;
    let fromHashId = localStorage.getItem('hashId');
    let token = localStorage.getItem('token');
    let code = window.atob(window.atob(token));
    let password = code.split(":")[1];
    let value = this.payment['price']['priceStart'];
    if (value <= 0 || value > this.blockchain.balance) {
      alert("Lượng tiền phải lớn hơn 0 và bé hơn số dư cho phép")
    } else {
      this.blockchain.web3.personal.unlockAccount(fromHashId, password);
      this.blockchain.web3.eth.sendTransaction(
        {
          from: fromHashId,
          to: self.blockchain.hashIdHospital,
          value: value * Math.pow(10, 18)
        }, function (error, result) {
          if (!error) {
            alert(`Bạn đã chuyển ${value} ETH từ ${fromHashId} đến ${self.blockchain.hashIdHospital}`);
            self.userService.update(`users/${self.idUser}/payment`, { status: 1 }, self.idPayment);
            self.router.navigateByUrl('/payment');
          }
          else {
            alert("Đã xảy ra lỗi, giao dịch không được thực hiện");
          }
        }
      )
    };
  }

  previous() {
    this.step--;
  }
  perchase() {
    confirm("Xác nhận chuyển 3.001 ETH đến bệnh viện ABC");
  }

}
