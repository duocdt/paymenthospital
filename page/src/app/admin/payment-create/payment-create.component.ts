import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.service';
import { Menu } from 'src/app/shared/class/menu';

@Component({
  selector: 'app-payment-create',
  templateUrl: './payment-create.component.html',
  styleUrls: ['./payment-create.component.scss']
})
export class PaymentCreateComponent implements OnInit {
  public user = [];
  constructor(
    public userService: UserService,
    public router: Router,
    public menu: Menu,
  ) { }

  ngOnInit() {
    this.menu.currentTitle = 'Tạo thanh toán';
    this.getItem();
  }

  getItem() {
    this.userService.getList().snapshotChanges().subscribe(
      item => {
        let length = item.length;
        for (let i = 0; i < length; i++) {
          if (item[i].payload.toJSON()['hash'] !== localStorage.getItem('hashId')) {
            let temp = item[i].payload.toJSON();
            temp['id'] = item[i].key;
            this.user.push(temp);
          }
        }
        console.log(this.user);

      })
  }
  gotoInfo(item) {
    this.router.navigateByUrl(`/admin/payment-create/${item.id}`);
  }
}
