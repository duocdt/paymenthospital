import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { FormControl } from '@angular/forms';
import { Menu } from 'src/app/shared/class/menu';

@Component({
  selector: 'app-payment-create-detail',
  templateUrl: './payment-create-detail.component.html',
  styleUrls: ['./payment-create-detail.component.scss']
})
export class PaymentCreateDetailComponent implements OnInit {
  public idUser;
  public listPayment = [];
  public newUser = {
    patient: {
      name: new FormControl(''),
      age: new FormControl(''),
      endDate: new FormControl(''),
      phone: new FormControl(''),
      reason: new FormControl(''),
      result: new FormControl(''),
      startDate: new FormControl(''),
      statusOut: new FormControl(''),
    },
    price: {
      priceStart: new FormControl('0'),
      fee: '0.00009',
      total: '',
   }
  }
  public total = this.newUser.price.priceStart.value + this.newUser.price.fee;
  constructor(
    public userService: UserService,
    private route: ActivatedRoute,
    public router: Router,
    public menu: Menu,
  ) { }

  ngOnInit() {
    this.menu.currentTitle = 'Thông tin tạo thanh toán';
    this.route.params.subscribe(params => {
      this.idUser = params['idUser'];
    });
    this.getItem();
  }
  getItem() {
    this.userService.getItem(`users/${this.idUser}/payment`).snapshotChanges().subscribe(
      data => {
        _.forEach(data.payload.toJSON(), (value, key) => {
          let t = value;
          t['id'] = key;
          this.listPayment.push(t);
        });
        console.log(this.listPayment);

      }
    )
  }

  add() {
    let body = {
      patient: {
        name: this.newUser.patient.name.value,
        age:this.newUser.patient.age.value,
        endDate: this.newUser.patient.endDate.value,
        phone: this.newUser.patient.phone.value,
        reason: this.newUser.patient.reason.value,
        result: this.newUser.patient.result.value,
        startDate: this.newUser.patient.startDate.value,
        statusOut: this.newUser.patient.statusOut.value,
      },
      price: {
        priceStart: this.newUser.price.priceStart.value,
        fee: 0.00009,
        total: Number(this.newUser.price.priceStart.value) + Number(this.newUser.price.fee),
     }, 
     status: 0
    }
    
    this.userService.create(`users/${this.idUser}/payment`,body);
   
  }
}
