import { Component, OnInit } from '@angular/core';
import {Menu} from './../shared/class/menu';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss']
})
export class RecordComponent implements OnInit {
  public user;
  constructor(
    public menu: Menu,
    public userService: UserService

  ) { }

  ngOnInit() {
    this.menu.currentTitle = 'Hồ sơ';
this.getItem();

  }

  getItem() {
    this.userService.getList().snapshotChanges().subscribe(
      item => {
        let length = item.length;
        for (let i = 0; i < length; i++) {
          if (item[i].payload.toJSON()['hash'] === localStorage.getItem('hashId')) {
            this.user = item[i].payload.toJSON();
            this.user.key = item[i]['key'];
            break;
          }
        }
      })
  }

}
